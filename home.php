<?php
	session_start();
	if($_SESSION['isLogin'] != true || $_SESSION['jam_selesai']==date("Y-m-d H:i:s"))
	{
		header("Location: form_login.php?message=nologin");
	}
	echo "Selamat datang, ",strtoupper($_SESSION['uname']),"<br> login pada: ",$_SESSION['jam_mulai'];
	echo "<br>";
	echo "<br>";
?>

	<!-- 
		tugas 2:
		tambahkan form yang method-nya post ke halaman simpandata.php
		nama lengkap: text
		username : ambil dari session
		alamat: textarea
		jenis kelamin: radio, walaupun pilihannya 2, name tetap 1
		hobi: checkbox -> diisi terserah minimal 3 pilihan, pilihannya 3, name juga 3
		pekerjaan: select -> diisi terserah minimal 3 pilihan, sama kayak radio
		tombol simpan
		kalo sudah klik simpan, tampilkan hasil inputannya
	-->
	
	<!DOCTYPE html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>FORM</title>
		<style>
			a:link, a:visited {
			background-color: #4CAF50;
			color: white;
			padding: 5px 10px;
			text-align: center;
			text-decoration: none;
			font-size: 16px;
			margin: 4px 2px;
			display: inline-block;
			}

			a:hover, a:active {
			background-color: red;
			}

			.button {
			background-color: #4CAF50; 
			border: none;
			color: white;
			padding: 5px 10px;
			text-align: center;
			text-decoration: none;
			display: inline-block;
			font-size: 16px;
			margin: 4px 2px;
			cursor: pointer;
			}
			.button1 {background-color: #008CBA;}
		</style>
	</head>
	<body>
	<form method="POST" action="simpandata.php" >
		<table>
			<tr>
				<td>Nama Lengkap </td>
				<td>:</td>
				<td><input type="text" name="nama" size="25"></td>
			</tr>
			<tr>
				<td>Alamat</td>
				<td>:</td>
				<td>
					<textarea name="alamat" size="25"></textarea>
				</td>
			</tr>
			<tr>
				<td>Jenis Kelamin</td>
				<td>:</td>
				<td>
					<input type="radio" name="jenis_kelamin" value="Laki-Laki" checked="" >Laki-Laki
					<input type="radio" name="jenis_kelamin" value="Perempuan" checked="" >Perempuan
				</td>
			</tr>
			<tr>
				<td>Hobi</td>
				<td>:</td>
				<td>
					<input type="checkbox" name="hobi" value="Sepak Bola">Sepak Bola
					<input type="checkbox" name="hobi" value="Berenang">Berenang
					<input type="checkbox" name="hobi" value="Membaca">Membaca
				</td>
			</tr>
			<tr>
				<td>Pekerjaan</td>
				<td>:</td>
				<td>
					<select name="pekerjaan" >
						<option value="Mahasiswa/Pelajar">Mahasiswa/Pelajar</option>
						<option value="Guru/PNS">Guru/PNS</option>
						<option value="Pegawai Swasta">Pegawai Swasta</option>
						<option value="Wiraswasta">Wiraswasta</option>
					</select>
				</td>
			</tr>
			
		</table>
		<tr>
			<td>
				<button class="button button2" type="submit" name="spn" value="submit">Simpan</button>
			</td>
		</tr>
		<tr>
			<td>
				<a href="logout.php">Logout</a>
			</td>
		</tr>
        </form>	
	</body>
	</html>
	
	
	
	
	
	
	